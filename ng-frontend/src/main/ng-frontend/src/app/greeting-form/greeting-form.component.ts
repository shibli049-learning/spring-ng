import { Component, OnInit } from '@angular/core';

import { Greeting }    from '../greeting';

@Component({
  selector: 'app-greeting-form',
  templateUrl: './greeting-form.component.html',
  styleUrls: ['./greeting-form.component.css']
})
export class GreetingFormComponent{

  powers = ['Really Smart', 'Super Flexible',
    'Super Hot', 'Weather Changer'];

  model = new Greeting("shibli");

  submitted = false;

  onSubmit() { this.submitted = true; }

  // TODO: Remove this when we're dodne
  get diagnostic() { return JSON.stringify(this.model); }

}
