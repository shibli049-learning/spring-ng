package com.shibli049.springng.backend.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by github.com/shibli049 on 5/14/18.
 */

@RestController
@RequestMapping("/api")
public class HelloController {

  @GetMapping("/hello")
  public String greet(@RequestParam(name = "name", required = false) String name){
    String msgFormat = "hello %s from REST";
    if(name == null || name.isEmpty()){
      name = "World";
    }

    return String.format(msgFormat, name);
  }

}
